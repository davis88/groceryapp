(function(){
    angular
        .module("GROCERYApp")
        .service("GROCERYAppAPI", [
            '$http',
            GROCERYAppAPI
        ]);
    
    function GROCERYAppAPI($http){
        var self = this;

        // query string
        self.searchGroceries = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/groceries?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        self.getGrocery = function(id){
            console.log(id);
            return $http.get("/api/groceries/" + id)
        }

        self.updateGrocery = function(grocery){
            console.log(grocery);
            return $http.put("/api/groceries",grocery);
        }

        self.deleteGrocery = function(id){
            console.log(id);
            return $http.delete("/api/groceries/"+ id);
        }
        
        // parameterized values
        /*
        self.searchGroceries = function(value){
            return $http.get("/api/groceries/" + value);
        }*/

        // post by body over request
        self.addGrocery = function(grocery){
            return $http.post("/api/groceries", grocery);
        }
    }
})();