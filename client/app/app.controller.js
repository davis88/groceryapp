(function () {
    angular
        .module("GROCERYApp")
        .controller("GroceryController", GroceryController)
        .controller("EditGroceryCtrl", EditGroceryCtrl)
        .controller("AddGroceryCtrl", AddGroceryCtrl)
        .controller("DeleteGroceryCtrl", DeleteGroceryCtrl);
        
    GroceryController.$inject = ['GROCERYAppAPI', '$uibModal', '$document', '$rootScope', '$scope', '$state'];
    EditGroceryCtrl.$inject = ['GROCERYAppAPI', '$rootScope', '$scope', '$state','$stateParams'];
    AddGroceryCtrl.$inject = ['GROCERYAppAPI', '$rootScope', '$scope','$state'];
    DeleteGroceryCtrl.$inject = ['$uibModalInstance', 'GROCERYAppAPI', 'items', '$rootScope', '$scope'];
    
    function DeleteGroceryCtrl($uibModalInstance, GROCERYAppAPI, items, $rootScope, $scope){
        var self = this;

        self.deleteGrocery = deleteGrocery;

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        console.log(items);
        GROCERYAppAPI.getGrocery(items).then((result)=>{
            console.log(result.data);
            self.grocery =  result.data;
        });

        function deleteGrocery(){
            console.log("delete grocery ...");
            GROCERYAppAPI.deleteGrocery(self.grocery.id).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshGroceryList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }

    function AddGroceryCtrl(GROCERYAppAPI, $rootScope, $scope, $state){
        console.log("Add Grocery");
        var self = this;
        self.saveGrocery = saveGrocery;

        self.grocery = {}

        function saveGrocery(){
            console.log("save grocery ...");
            console.log(self.grocery.name);
            console.log(self.grocery.brand);

            GROCERYAppAPI.addGrocery(self.grocery).then((result)=>{
                //console.log(result);
                console.log("Add grocery -> " + result.id);
                $rootScope.$broadcast('refreshGroceryListFromAdd', result.data);
                // $state.go('home') This to redirect once saved successfully
             }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
             })
            $state.go('home')
            // $uibModalInstance.close(self.run);
        }
    }
        

    
    function EditGroceryCtrl( GROCERYAppAPI, $rootScope, $scope, $state, $stateParams){
        console.log("Edit Grocery Ctrl");
        var self = this;
        self.id = $stateParams.id;
        id = $stateParams.id;

        GROCERYAppAPI.getGrocery(id).then((result)=>{
           console.log(result.data);
           self.grocery =  result.data;
        })

        self.saveGrocery = saveGrocery;

        function saveGrocery(){
            console.log("save grocery ...");
            console.log(self.grocery.name);
            console.log(self.grocery.brand);
            GROCERYAppAPI.updateGrocery(self.grocery).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshGroceryList');
             }).catch((error)=>{
                console.log(error);
             })
            // $uibModalInstance.close(self.run);
            $state.go('home')
        }

    }

    function GroceryController(GROCERYAppAPI, $uibModal, $document, $rootScope, $scope,$state) {
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        self.groceries = [];
        self.maxsize=5;
        self.totalItems = 0;
        self.itemsPerPage = 20;
        self.currentPage = 1;

        self.searchGroceries =  searchGroceries;
        self.addGrocery =  addGrocery;
        self.editGrocery = editGrocery;
        self.deleteGrocery = deleteGrocery;
        self.pageChanged = pageChanged;

        function searchAllGroceries(searchKeyword,orderby,itemsPerPage,currentPage){
            GROCERYAppAPI.searchGroceries(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                self.groceries = results.data.rows;
                self.totalItems = results.data.count;
                $scope.numPages = Math.ceil(self.totalItems /self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });
        }


        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllGroceries(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshGroceryList",function(){
            console.log("refresh grocery list "+ self.searchKeyword);
            searchAllGroceries(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshGroceryListFromAdd",function(event, args){
            console.log("refresh grocery list from id"+ args.id);
            var groceries = [];
            groceries.push(args);
            self.searchKeyword = "";
            self.groceries = groceries;
        });

        function searchGroceries(){
            console.log("search groceries  ....");
            console.log(self.orderby);
            searchAllGroceries(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        }

        function addGrocery(size, parentSelector){
            console.log("post add grocery  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/addGrocery.html',
                controller: 'AddGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function editGrocery(id, size, parentSelector){
            $state.go('edit', {'id': id})
        }

        function deleteGrocery(id, size, parentSelector){
            console.log("delete Grocery...");
            console.log("id > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/deleteGrocery.html',
                controller: 'DeleteGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }
    }
})();