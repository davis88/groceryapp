(function () {  
    
    angular
    .module("GROCERYApp")
    .config(uirouterAppConfig);
    uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];
    
    
    function uirouterAppConfig($stateProvider, $urlRouterProvider){
    
    $stateProvider
        .state("home",{
            url : '/home',
            templateUrl: "app/home.html",
            controller : 'GroceryController',
            controllerAs : 'ctrl'
        })
        .state("add", {
            url: "/add",
            templateUrl: "app/addGrocery.html",
            controller : 'AddGroceryCtrl',
            controllerAs : 'ctrl'
        })
        .state("edit", {
            url: "/edit",
            templateUrl: "app/editGrocery.html",
            controller : 'EditGroceryCtrl',
            controllerAs : 'ctrl',
            params: {
                'id': '',
            }
        })
    
        $urlRouterProvider.otherwise("/home"); //if can't find any route, it goes to /home
    
    }
    
    })();