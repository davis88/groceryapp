-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2016 at 05:22 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grocery`
--

-- --------------------------------------------------------

--
-- Table structure for table `grocery_list`
--

CREATE TABLE `grocery_list` (
  `groc_no` int(11) NOT NULL,
  `upc12` bigint(12) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grocery_list`
--

INSERT INTO `grocery_list` (`groc_no`, `upc12`, `brand`, `name`) VALUES
(1, 35200264013, 'Riceland', 'Riceland American Jazmine Rice'),
(2, 11111065925, 'Caress', 'Caress Velvet Bliss Ultra Silkening Beauty Bar - 6 Ct'),
(3, 23923330139, 'Earth\'s Best', 'Earth\'s Best Organic Fruit Yogurt Smoothie Mixed Berry'),
(4, 208529000000, 'Boar\'s Head', 'Boar\'s Head Sliced White American Cheese - 120 Ct'),
(5, 759283000000, 'Back To Nature', 'Back To Nature Gluten Free White Cheddar Rice Thin Crackers'),
(6, 74170388732, 'Sally Hansen', 'Sally Hansen Nail Color Magnetic 903 Silver Elements'),
(7, 70177154004, 'Twinings Of London', 'Twinings Of London Classics Lady Grey Tea - 20 Ct'),
(8, 51600080015, 'Lea & Perrins', 'Lea & Perrins Marinade In-a-bag Cracked Peppercorn'),
(9, 19600923015, 'Van De Kamp\'s', 'Van De Kamp\'s Fillets Beer Battered - 10 Ct'),
(10, 688267000000, 'Ahold', 'Ahold Cocoa Almonds'),
(11, 657623000000, 'Honest Tea', 'Honest Tea Peach White Tea'),
(12, 74676640211, 'Mueller', 'Mueller Sport Care Basic Support Level Medium Elastic Knee Support'),
(13, 603084000000, 'Garnier Nutritioniste', 'Garnier Nutritioniste Moisture Rescue Fresh Cleansing Foam'),
(14, 41167300121, 'Pamprin', 'Pamprin Maximum Strength Multi-symptom Menstrual Pain Relief'),
(15, 79400847201, 'Suave', 'Suave Naturals Moisturizing Body Wash Creamy Tropical Coconut'),
(16, 792850000000, 'Burt\'s Bees', 'Burt\'s Bees Daily Moisturizing Cream Sensitive'),
(17, 88313590791, 'Ducal', 'Ducal Refried Red Beans'),
(18, 21200725340, 'Scotch', 'Scotch Removable Clear Mounting Squares - 35 Ct'),
(19, 41520035646, 'Careone', 'Careone Family Comb Set - 8 Ct'),
(20, 204040000000, 'Usda Produce', 'Plums Black'), etc ...