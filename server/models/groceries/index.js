// CREATE TABLE `grocery` (
//     `id` int(11) NOT NULL,
//     `birth_date` date NULL,
//     `name` varchar(14) NOT NULL,
//     `brand` varchar(16) NULL,
//     `gender` enum('M','F') NOT NULL,
//     `registration_date` date NOT NULL,
//     `upc12` varchar(1000) DEFAULT NULL,
//     PRIMARY KEY (`id`)
//   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

module.exports = function(connection, Sequelize){
    var Grocery = connection.define('groceries', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        brand:{
            type: Sequelize.STRING,
            allowNull: false
        },
        upc12: {
            type: Sequelize.BIGINT,
            allowNull: false
        }
    }, {
        timestamps: false,
        tableName: 'grocery_list'
    });
    return Grocery;
}